const memoization = require('./memoizaton');
const expect = require('chai').expect;
const sinon = require('sinon');

describe('memoization', function () {
    it('should memoize function result I', () => {
        let returnValue = 5;
        const testFunction = () => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        returnValue = 10;

        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);
    });

    it('should memoize function result II', () => {
        const addToTimeFn = (year, month, day) => Date.now() + new Date(year, month, day).getTime();
        const resolverFn = (year, month, day) => year + month + day;

        const year = 2020;
        const month = 11;
        const day = 1;

        const sandbox = sinon.createSandbox();
        sandbox.spy(addToTimeFn, 'apply');
        const clock = sinon.useFakeTimers(new Date().getTime());

        const memoized = memoization.memoize(addToTimeFn, resolverFn, 5000);

        memoized(year, month, day);

        // The function has been called only once (result is saved to cache)
        expect(addToTimeFn.apply.calledOnce).to.be.true;

        memoized(year, month, day);

        // The function has not been called again (therefore the result is returned from the cache)
        expect(addToTimeFn.apply.calledOnce).to.be.true;

        // Fast forward
        clock.tick(5000);

        memoized(year, month, day);

        // The function has been called exactly twice, as the cache was cleared after 5000 ms
        expect(addToTimeFn.apply.calledTwice).to.be.true;

        clock.restore();
        sandbox.restore();
    });

    // "it should also work with other types then strings, if there are limitations to which
    // types are possible please state them"
    it('should test memoization for various data types', () => {
        const fn = () => 4711;
        const memoized = memoization.memoize(fn, (key) => key, 1000);

        let sandbox = sinon.createSandbox();
        sandbox.spy(fn, 'apply');

        // The limits arise when using objects or similar as keys. The accessor (key) can't distinguish
        // between one object or another and returns a wrong, cached result:
        memoized({ hello: 'there' });
        expect(fn.apply.calledOnce).to.be.true;
        memoized({ hello2: 'there2' });
        // This actually should be called twice, but as mentioned earlier, it returns the previously
        // cached object and is therefore false.
        expect(fn.apply.calledTwice).to.be.false;
        // Note: Same goes for Maps and Sets
        // A workaround could be by using the `JSON.stringify()` function to create the keys

        sandbox.restore();
        sandbox = sinon.createSandbox();
        sandbox.spy(fn, 'apply');

        // Arrays on the other hand apply the `.toString()` method when being used as a key, and
        // are therefore working: 
        memoized(['hello', 'there']); // Saving to cache
        expect(fn.apply.calledOnce).to.be.true;
        memoized(['hello2', 'there2']); // Also saving to cache
        expect(fn.apply.calledTwice).to.be.true;
        memoized(['hello2', 'there2']); // Reading the correct value from cache
        expect(fn.apply.calledTwice).to.be.true;
        // Note: Other data types like String, Number, Boolean or Symbol also work as expected
        // (But careful, even `null`, `undefined`, `''` work in this case)

        sandbox.restore();
    });
});