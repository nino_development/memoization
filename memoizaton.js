/**
 * Creates a function that memoizes the result of func. If resolver is provided,
 * it determines the cache key for storing the result based on the arguments provided to the memorized function.
 * By default, the first argument provided to the memorized function is used as the map cache key. The memorized values
 * timeout after the timeout exceeds. The timeout is in defined in milliseconds.
 *
 * Example:
 * function addToTime(year, month, day) {
 *  return Date.now() + Date(year, month, day);
 * }
 *
 * const memoized = memoization.memoize(addToTime, (year, month, day) => year + month + day, 5000)
 *
 * // call the provided function cache the result and return the value
 * const result = memoized(1, 11, 26); // result = 1534252012350
 *
 * 
 * ######################
 * ### NOTE FROM NINO ###
 * ######################
 * 
 * I think the return/result value above is expected when using `return Date.now() + year + month + day` in the
 * `addToTime()` function. Not really able to reproduce it accurately, but with the current date (2020-07-20) it
 * seems pretty close (1595271424571). For clarification: I used `return Date.now() + new Date(year, month, day).getTime()`
 * in the `addToTime()` function instead. That's why the results may differ more significantly.
 *
 * ######################
 * ###### END NOTE ######
 * ######################
 * 
 * 
 * // because there was no timeout this call should return the memorized value from the first call
 * const secondResult = memoized(1, 11, 26); // secondResult = 1534252012350
 *
 * // after 5000 ms the value is not valid anymore and the original function should be called again
 * const thirdResult = memoized(1, 11, 26); // thirdResult = 1534252159271
 *
 * @param func      the function for which the return values should be cached
 * @param resolver  if provided gets called for each function call with the exact same set of parameters as the
 *                  original function, the resolver function should provide the memoization key.
 * @param timeout   timeout for cached values in milliseconds
 */
function memoize(func, resolver, timeout = 5000) {
    if (typeof func === 'function') {
        // Initialize cache in parent scope
        let cache = {};

        // Use spread operator on `args`, as it is more convenient than parsing the `arguments` object
        return function (...args) {
            // "If resolver is provided, it determines the cache key for storing the result based on the arguments
            // provided to the memorized function. By default, the first argument provided to the memorized function
            // is used as the map cache key."
            const key = typeof resolver === 'function' ? resolver.apply(this, args) : args[0];

            if (cache[key]) {
                // Return cached value if it exists in the cache
                return cache[key];
            } else {
                // Apply function and save result to cache
                const res = func.apply(this, args);
                cache[key] = res;

                // "The memorized values timeout after the timeout exceeds."
                setTimeout(() => {
                    // Remove property from `cache` object
                    delete cache[key];
                }, timeout);

                return res;
            }
        }
    }
}

/*
 * A little side note: This memoize function returns a new function with "enhanced capabilities" (higher-order function).
 * I used a similar construct (Render Props) in previous React projects to provide data to various components:
 * 
 * <GeoLocationProvider>
 *   {(geoLocation) => (
 *     <div>...</div>
 *   )}
 * </GeoLocationProvider>
 */

module.exports = {
    memoize,
};